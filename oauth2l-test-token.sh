#!/bin/bash

# If your service_account.json is correct you should definitely be able to get a token
# using Google's official tool. If this fails, it will definitely fail with our own.

set -e

rm -f ~/.oauth2l

export GOOGLE_APPLICATION_CREDENTIALS=$HOME/Downloads/service_account.json
PROJECT=even-flight-244020

token=$(oauth2l fetch ndev.clouddns.readwrite)
echo API Token: $token
curl -fL "https://www.googleapis.com/dns/v1/projects/$PROJECT/managedZones" -H "Authorization: Bearer $token"

#token=$(oauth2l fetch --jwt ndev.clouddns.readwrite)
#token=$(oauth2l fetch --jwt https://www.googleapis.com/auth/ndev.clouddns.readwrite)
#echo JWT: $token
#curl -fL "https://www.googleapis.com/dns/v1/projects/$PROJECT/managedZones" -H "Authorization: Bearer $token"
