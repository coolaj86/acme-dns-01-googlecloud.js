# acme-dns-01-googlecloud.js

Google Domains + Let's Encrypt for Node.js - ACME dns-01 challenges w/ ACME.js and Greenlock.js

In Progress. Would love help. Please contact @coolaj86 on Keybase.

-   [x] zones
-   [ ] set
-   [ ] get
-   [ ] remove

Implementation Details

-   https://cloud.google.com/dns/docs/reference/v1/
-   https://cloud.google.com/service-usage/docs/getting-started#api
-   https://github.com/google/oauth2l

## Authenticating

Google has made things _way_ too complicated.

-   Create a project
-   Create a Service Account and download the `service_account.json`
-   Create a managed public zone for your domain: <https://cloud.google.com/dns/docs/quickstart#create_a_managed_public_zone>

# Test This First!

Edit the file `oauth2l-test-token.sh`

Change the location of `service_account.json` to whatever it needs to be.

Change the `PROJECT` to the name of your project.

If that doesn't work, something is wrong with your credentials, nothing else will work.

# Usage

First you create an instance with your credentials:

```js
var dns01 = require('acme-dns-01-googlecloud').create({
	baseUrl: 'https://www.googleapis.com/dns/v1/', // default

	// contains private_key, private_key_id, project_id, and client_email
	serviceAccountPath: __dirname + '/service_account.json'
});
```
