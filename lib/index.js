'use strict';

//var auth = require('./auth.js');
var defaults = {
	baseUrl: 'https://www.googleapis.com/dns/v1/'
};

module.exports.create = function(config) {
	var request;
	var baseUrl = (config.baseUrl || defaults.baseUrl).replace(/\/$/, '');
	var token = config.token;
	var sa = getServiceAccount(config);

	return {
		init: function(opts) {
			request = opts.request;
			return null;
		},
		zones: function(data) {
			//console.info('List Zones', data);
			return api({
				url: baseUrl + '/projects/' + sa.project_id + '/managedZones',
				json: true
			}).then(function(resp) {
				return resp.body.managedZones.map(function(zone) {
					// slice out the leading and trailing single quotes, and the trailing dot
					// (assuming that all 'dnsName's probably look the same)
					return zone.dnsName.slice(1, zone.dnsName.length - 2);
				});
			});
		},
		set: function(data) {
			// console.info('Add TXT', data);
			throw Error('setting TXT not implemented');
		},
		remove: function(data) {
			// console.info('Remove TXT', data);
			throw Error('removing TXT not implemented');
		},
		get: function(data) {
			// console.info('List TXT', data);
			throw Error('listing TXTs not implemented');
		}
	};

	function api(opts) {
		//return auth.getToken(sa).then(function(token) {
		opts.headers = opts.headers || {};
		opts.headers.Authorization = 'Bearer ' + token;
		return request(opts);
		//});
	}

	function getServiceAccount(config) {
		var saPath =
			config.serviceAccountPath ||
			process.env.GOOGLE_APPLICATION_CREDENTIALS;
		var sa = config.serviceAccount || require(saPath);

		if (
			!sa ||
			!(
				sa.private_key &&
				sa.private_key_id &&
				sa.client_email &&
				sa.project_id
			)
		) {
			throw new Error(
				'missing or incomplete service_account.json: set serviceAccount serviceAccountPath'
			);
		}
		return sa;
	}
};
