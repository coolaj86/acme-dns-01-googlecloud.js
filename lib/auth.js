'use strict';

var Keypairs = require('keypairs');

module.exports.getToken = function(serviceAccount) {
	var jwt = '';
	var exp = 0;

	if (exp - Date.now() > 0) {
		return Promise.resolve(jwt);
	}

	return module.exports.generateToken(serviceAccount).then(function(_jwt) {
		jwt = _jwt;
		exp = Math.round(Date.now()) - 15 * 60 * 60 * 1000;
		return jwt;
	});
};

module.exports.generateToken = function(serviceAccount) {
	var sa = serviceAccount;
	return Keypairs.import({ pem: sa.private_key }).then(function(key) {
		return Keypairs.signJwt({
			jwk: key,
			iss: sa.client_email,
			exp: '1h',
			header: {
				kid: sa.private_key_id
			},
			claims: {
				//aud: 'ndev.clouddns.readwrite',
				aud: 'https://www.googleapis.com/auth/ndev.clouddns.readwrite',
				sub: sa.client_email
			}
		});
	});
};
