#!/usr/bin/env node
'use strict';

// See https://git.coolaj86.com/coolaj86/acme-challenge-test.js
var tester = require('acme-challenge-test');
require('dotenv').config();

// Usage: node ./test.js example.com xxxxxxxxx
var zone = process.argv[2] || process.env.ZONE;
var config = {
	serviceAccountPath:
		process.argv[3] || process.env.GOOGLE_APPLICATION_CREDENTIALS,
	token: process.argv[4] || process.env.TOKEN
};
var challenger = require('./index.js').create(config);

// Google has its own special authentication
var sa = require(config.serviceAccountPath);
require('./lib/auth.js')
	.getToken(sa)
	.then(function(/*jwt*/) {
		//console.info('\nAuthorization: Bearer ' + jwt + '\n');

		// The dry-run tests can pass on, literally, 'example.com'
		// but the integration tests require that you have control over the domain
		return tester
			.testZone('dns-01', zone, challenger)
			.then(function() {
				console.info('PASS', zone);
			})
			.catch(function(e) {
				console.error(e.message);
				console.error(e.stack);
			});
	})
	.catch(function(err) {
		console.error(err.stack);
	});
